# README #

This is for learning dynamics of objects using regression methods. We have implemented Linear Regression and Bayesian Regression.

Author: roya.sabbaghnovin@utah.edu

### How to set up? ###

- Put data files in the folder.

- Address them in "Data.py" file. Default is what is already in the folder.

- Choose method and model in the "Main.py" file as instructed in the comments.

- Run "Main.py" 


